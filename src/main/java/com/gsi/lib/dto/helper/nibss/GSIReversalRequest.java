/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsi.lib.dto.helper.nibss;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author chineduojiteli
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GSIReversalRequest {

    @JsonProperty("GSITransactionID")
    private String gsiTransactionID;
    @JsonProperty("ReversalReason")
    private String reversalReason;

    public String getGsiTransactionID() {
        return gsiTransactionID;
    }

    public void setGsiTransactionID(String gsiTransactionID) {
        this.gsiTransactionID = gsiTransactionID;
    }

    public String getReversalReason() {
        return reversalReason;
    }

    public void setReversalReason(String reversalReason) {
        this.reversalReason = reversalReason;
    }

}
