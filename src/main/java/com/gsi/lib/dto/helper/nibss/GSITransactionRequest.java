/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsi.lib.dto.helper.nibss;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author chineduojiteli
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GSITransactionRequest {

    @JsonProperty("Mandatereference")
    private String Mandatereference;
    @JsonProperty("RecoveryAmount")
    private String recoveryAmount;
    @JsonProperty("BvnNumber")
    private String bvnNumber;
    @JsonProperty("RecoveryAccountNumber")
    private String recoveryAccountNumber;
    @JsonProperty("InstitutionCode")
    private String institutionCode;
    @JsonProperty("MandateTypeID")
    private Integer mandateTypeID;
    @JsonProperty("MandateCurrencyID")
    private Integer mandateCurrencyID;
    @JsonProperty("RCID")
    private String rcID;
    @JsonProperty("TinID")
    private String tinID;

    public String getMandatereference() {
        return Mandatereference;
    }

    public void setMandatereference(String Mandatereference) {
        this.Mandatereference = Mandatereference;
    }

    public String getRecoveryAmount() {
        return recoveryAmount;
    }

    public void setRecoveryAmount(String recoveryAmount) {
        this.recoveryAmount = recoveryAmount;
    }

    public String getBvnNumber() {
        return bvnNumber;
    }

    public void setBvnNumber(String bvnNumber) {
        this.bvnNumber = bvnNumber;
    }

    public String getRecoveryAccountNumber() {
        return recoveryAccountNumber;
    }

    public void setRecoveryAccountNumber(String recoveryAccountNumber) {
        this.recoveryAccountNumber = recoveryAccountNumber;
    }

    public String getInstitutionCode() {
        return institutionCode;
    }

    public void setInstitutionCode(String institutionCode) {
        this.institutionCode = institutionCode;
    }

    public Integer getMandateTypeID() {
        return mandateTypeID;
    }

    public void setMandateTypeID(Integer mandateTypeID) {
        this.mandateTypeID = mandateTypeID;
    }

    public Integer getMandateCurrencyID() {
        return mandateCurrencyID;
    }

    public void setMandateCurrencyID(Integer mandateCurrencyID) {
        this.mandateCurrencyID = mandateCurrencyID;
    }

    public String getRcID() {
        return rcID;
    }

    public void setRcID(String rcID) {
        this.rcID = rcID;
    }

    public String getTinID() {
        return tinID;
    }

    public void setTinID(String tinID) {
        this.tinID = tinID;
    }
    
    
}
