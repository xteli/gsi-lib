/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsi.lib.dto.helper.nibss;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author chineduojiteli
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Reversal {

    @JsonProperty("NipTransactionReference")
    private String nipTransactionReference;
    @JsonProperty("DRSStatus")
    private String drsStatus;
    @JsonProperty("DRSMessage")
    private String drsMessage;

    public String getNipTransactionReference() {
        return nipTransactionReference;
    }

    public void setNipTransactionReference(String nipTransactionReference) {
        this.nipTransactionReference = nipTransactionReference;
    }

    public String getDrsStatus() {
        return drsStatus;
    }

    public void setDrsStatus(String drsStatus) {
        this.drsStatus = drsStatus;
    }

    public String getDrsMessage() {
        return drsMessage;
    }

    public void setDrsMessage(String drsMessage) {
        this.drsMessage = drsMessage;
    }
    
    
}
