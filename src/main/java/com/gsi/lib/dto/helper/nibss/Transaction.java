/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsi.lib.dto.helper.nibss;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author chineduojiteli
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Transaction {

    @JsonProperty("NipTransactionReference")
    private String nipTransactionReference;
    @JsonProperty("CreditAcountnumber")
    private String creditAcountnumber;
    @JsonProperty("CreditAcountname")
    private String creditAcountname;
    @JsonProperty("CreditAcountCurrency")
    private String creditAcountCurrency;
    @JsonProperty("DebitAccountname")
    private String debitAccountname;
    @JsonProperty("DebitAccountnumber")
    private String debitAccountnumber;
    @JsonProperty("DebitAccountCurrency")
    private String debitAccountCurrency;
    @JsonProperty("Amount")
    private String amount;
    @JsonProperty("ConversionRate")
    private String conversionRate;
    @JsonProperty("SettlementAmount")
    private String settlementAmount;
    @JsonProperty("DebitStatus")
    private String debitStatus;
    @JsonProperty("CreditStatus")
    private String creditStatus;
    @JsonProperty("PaymentReference")
    private String paymentReference;
    @JsonProperty("NipResponseCode")
    private String nipResponseCode;
    @JsonProperty("Narration")
    private String narration;
    @JsonProperty("DestinationBankcode")
    private String destinationBankcode;
    @JsonProperty("SourceBankCode")
    private String sourceBankCode;

    public String getNipTransactionReference() {
        return nipTransactionReference;
    }

    public void setNipTransactionReference(String nipTransactionReference) {
        this.nipTransactionReference = nipTransactionReference;
    }

    public String getCreditAcountnumber() {
        return creditAcountnumber;
    }

    public void setCreditAcountnumber(String creditAcountnumber) {
        this.creditAcountnumber = creditAcountnumber;
    }

    public String getCreditAcountname() {
        return creditAcountname;
    }

    public void setCreditAcountname(String creditAcountname) {
        this.creditAcountname = creditAcountname;
    }

    public String getCreditAcountCurrency() {
        return creditAcountCurrency;
    }

    public void setCreditAcountCurrency(String creditAcountCurrency) {
        this.creditAcountCurrency = creditAcountCurrency;
    }

    public String getDebitAccountname() {
        return debitAccountname;
    }

    public void setDebitAccountname(String debitAccountname) {
        this.debitAccountname = debitAccountname;
    }

    public String getDebitAccountnumber() {
        return debitAccountnumber;
    }

    public void setDebitAccountnumber(String debitAccountnumber) {
        this.debitAccountnumber = debitAccountnumber;
    }

    public String getDebitAccountCurrency() {
        return debitAccountCurrency;
    }

    public void setDebitAccountCurrency(String debitAccountCurrency) {
        this.debitAccountCurrency = debitAccountCurrency;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getConversionRate() {
        return conversionRate;
    }

    public void setConversionRate(String conversionRate) {
        this.conversionRate = conversionRate;
    }

    public String getSettlementAmount() {
        return settlementAmount;
    }

    public void setSettlementAmount(String settlementAmount) {
        this.settlementAmount = settlementAmount;
    }

    public String getDebitStatus() {
        return debitStatus;
    }

    public void setDebitStatus(String debitStatus) {
        this.debitStatus = debitStatus;
    }

    public String getCreditStatus() {
        return creditStatus;
    }

    public void setCreditStatus(String creditStatus) {
        this.creditStatus = creditStatus;
    }

    public String getPaymentReference() {
        return paymentReference;
    }

    public void setPaymentReference(String paymentReference) {
        this.paymentReference = paymentReference;
    }

    public String getNipResponseCode() {
        return nipResponseCode;
    }

    public void setNipResponseCode(String nipResponseCode) {
        this.nipResponseCode = nipResponseCode;
    }

    public String getNarration() {
        return narration;
    }

    public void setNarration(String narration) {
        this.narration = narration;
    }

    public String getDestinationBankcode() {
        return destinationBankcode;
    }

    public void setDestinationBankcode(String destinationBankcode) {
        this.destinationBankcode = destinationBankcode;
    }

    public String getSourceBankCode() {
        return sourceBankCode;
    }

    public void setSourceBankCode(String sourceBankCode) {
        this.sourceBankCode = sourceBankCode;
    }

}
