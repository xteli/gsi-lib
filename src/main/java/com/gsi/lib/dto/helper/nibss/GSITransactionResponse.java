/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsi.lib.dto.helper.nibss;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 *
 * @author chineduojiteli
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GSITransactionResponse {

    @JsonProperty("TransactionReports")
    private List<Transaction> transactionReports;
    @JsonProperty("ResponseCode")
    private String responseCode;
    @JsonProperty("ResponseDescription")
    private String responseDescription;
    @JsonProperty("NIPResponseCode")
    private String nipResponseCode;
    @JsonProperty("GsiTransactonID")
    private String gsiTransactonID;

    public List<Transaction> getTransactionReports() {
        return transactionReports;
    }

    public void setTransactionReports(List<Transaction> transactionReports) {
        this.transactionReports = transactionReports;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseDescription() {
        return responseDescription;
    }

    public void setResponseDescription(String responseDescription) {
        this.responseDescription = responseDescription;
    }

    public String getNipResponseCode() {
        return nipResponseCode;
    }

    public void setNipResponseCode(String nipResponseCode) {
        this.nipResponseCode = nipResponseCode;
    }

    public String getGsiTransactonID() {
        return gsiTransactonID;
    }

    public void setGsiTransactonID(String gsiTransactonID) {
        this.gsiTransactonID = gsiTransactonID;
    }

}
