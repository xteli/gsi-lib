/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsi.lib.dto.helper;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.sql.Timestamp;

/**
 *
 * @author chineduojiteli
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MandateConfig {

    private int id;
    private String title;
    private String maturityDays;
    private String transactionIntervalHours;
    @JsonSerialize(using = CustomDateSerializer.class)
    @JsonIgnore
    private Timestamp createdOn;
    @JsonSerialize(using = CustomDateSerializer.class)
    @JsonIgnore
    private Timestamp lastUpdatedOn;
    private int status;
    @JsonIgnore
    private int createdBy;
    @JsonIgnore
    private int lastUpdatedBy;
    @JsonIgnore
    private String updatedBy;
    @JsonIgnore
    private String creator;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMaturityDays() {
        return maturityDays;
    }

    public void setMaturityDays(String maturityDays) {
        this.maturityDays = maturityDays;
    }

    public String getTransactionIntervalHours() {
        return transactionIntervalHours;
    }

    public void setTransactionIntervalHours(String transactionIntervalHours) {
        this.transactionIntervalHours = transactionIntervalHours;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public Timestamp getLastUpdatedOn() {
        return lastUpdatedOn;
    }

    public void setLastUpdatedOn(Timestamp lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    public int getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(int lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

}
