/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsi.lib.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 *
 * @author chineduojiteli
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GSISRequest {

    //for querying records
    private String startDate;
    private String endDate;

    //for setting up GSI Transaction
    private String transactionRef;

    //for setting up GSI Mandate
    private String mandateRef;
    private int mandateType;
    private String subjectId;
    //   private String currencyCode;
    private int currencyId;
    private String corporateId;
    private String corporateType;
    private String repaymentBankCode;
    private String repaymentAcctNumber;
    //for credit
    private String amount;
    private String crmsRef;

    private String comment;

    //upload file details
    private String fileName;
    private String fileExtension;
    private String fileBase64;

    //actors
    private String createdBy;
    private String modifiedBy;
    private String approvedBy;

    private int action;

    //transaction
    public String getMandateRef() {
        return mandateRef;
    }

    public void setMandateRef(String mandateRef) {
        this.mandateRef = mandateRef;
    }

    public int getMandateType() {
        return mandateType;
    }

    public void setMandateType(int mandateType) {
        this.mandateType = mandateType;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

//    public String getCurrencyCode() {
//        return currencyCode;
//    }
//
//    public void setCurrencyCode(String currencyCode) {
//        this.currencyCode = currencyCode;
//    }

    public String getCorporateId() {
        return corporateId;
    }

    public void setCorporateId(String corporateId) {
        this.corporateId = corporateId;
    }

    public String getCorporateType() {
        return corporateType;
    }

    public void setCorporateType(String corporateType) {
        this.corporateType = corporateType;
    }

    public String getRepaymentBankCode() {
        return repaymentBankCode;
    }

    public void setRepaymentBankCode(String repaymentBankCode) {
        this.repaymentBankCode = repaymentBankCode;
    }

    public String getRepaymentAcctNumber() {
        return repaymentAcctNumber;
    }

    public void setRepaymentAcctNumber(String repaymentAcctNumber) {
        this.repaymentAcctNumber = repaymentAcctNumber;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCrmsRef() {
        return crmsRef;
    }

    public void setCrmsRef(String crmsRef) {
        this.crmsRef = crmsRef;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileExtension() {
        return fileExtension;
    }

    public void setFileExtension(String fileExtension) {
        this.fileExtension = fileExtension;
    }

    public String getFileBase64() {
        return fileBase64;
    }

    public void setFileBase64(String fileBase64) {
        this.fileBase64 = fileBase64;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }

    public String getTransactionRef() {
        return transactionRef;
    }

    public void setTransactionRef(String transactionRef) {
        this.transactionRef = transactionRef;
    }

    public int getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(int currencyId) {
        this.currencyId = currencyId;
    }

}
