/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsi.lib.dbutil;

import com.gsi.lib.entities.Corporate;
import com.gsi.lib.entities.Currency;
import com.gsi.lib.entities.Institution;
import com.gsi.lib.entities.EntityApproval;
import com.gsi.lib.entities.GSIMandate;
import com.gsi.lib.entities.GSITransaction;
import com.gsi.lib.entities.Ping;
import com.gsi.lib.entities.SystemAudit;
import com.gsi.lib.util.Util;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

/**
 *
 * @author chineduojiteli
 */
public class HibernateUtil {

    private static SessionFactory SESSION_FACTORY;

    private HibernateUtil() {

    }

    public static SessionFactory getSessionFactory() {
        if (SESSION_FACTORY == null) {
            synchronized (HibernateUtil.class) {
                try {
                    Util config = new Util();
                    // final AnnotationConfiguration configuration = new AnnotationConfiguration();
                    AnnotationConfiguration configuration = new AnnotationConfiguration();
                    configuration.setProperty("hibernate.connection.driver_class", config.getParameter("hibernatedbClassDriver"));
                    configuration.setProperty("hibernate.connection.url", config.getParameter("hibernatedbUrl"));
                    configuration.setProperty("hibernate.connection.username", config.getParameter("hibernatedbUsername"));
                    configuration.setProperty("hibernate.connection.password", new Util(config.getParameter("encryptionKey")).decryptData(config.getParameter("hibernatedbPassword")));
                    configuration.setProperty("hibernate.dialect", config.getParameter("hibernateDialect"));
                    configuration.setProperty("hibernate.show_sql", config.getParameter("hibernateShowSql"));
                    configuration.setProperty("hibernate.hbm2ddl.auto", config.getParameter("hibernatehbm2ddl"));
                    configuration.setProperty("hibernate.current_session_context_class", "thread");

                    //beginning of connection pooling settings
                    configuration.setProperty("hibernate.c3p0.min_size", config.getParameter("hibernate.c3p0.min_size"));
                    configuration.setProperty("hibernate.c3p0.max_size", config.getParameter("hibernate.c3p0.max_size"));
                    configuration.setProperty("hibernate.c3p0.timeout", config.getParameter("hibernate.c3p0.timeout"));
                    configuration.setProperty("hibernate.c3p0.max_statements", config.getParameter("hibernate.c3p0.max_statements"));
                    configuration.setProperty("hibernate.c3p0.idle_test_period", config.getParameter("hibernate.c3p0.idle_test_period"));
                    configuration.setProperty("hibernate.connection.provider_class", config.getParameter("hibernate.connection.provider_class"));
                    //end of connection pooling settings
                    //load POJOs
                    configuration.addAnnotatedClass(SystemAudit.class);
                    configuration.addAnnotatedClass(Ping.class);
                    configuration.addAnnotatedClass(GSIMandate.class);
                    configuration.addAnnotatedClass(GSITransaction.class);
                    configuration.addAnnotatedClass(EntityApproval.class);
                    configuration.addAnnotatedClass(Institution.class);
                    configuration.addAnnotatedClass(Corporate.class);
                    configuration.addAnnotatedClass(Currency.class);
                    //end of load POJOs
                    SESSION_FACTORY = configuration.buildSessionFactory();
                    System.out.println("GSI Session Factory successfully built : " + SESSION_FACTORY.toString());
                } catch (Throwable e) {
                    System.err.println("Error in creating GSI Session Factory object."
                            + e.getMessage());
                    throw new ExceptionInInitializerError(e);
                }
            }

        }
        return SESSION_FACTORY;
    }

}
