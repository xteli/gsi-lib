/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsi.lib.entities;

import com.gsi.lib.util.Enum;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author chineduojiteli
 */
@Entity
@Table(name = "EntityApproval")
public class EntityApproval implements Serializable {

    public EntityApproval(String approvalCode) {
        this.approvalCode = approvalCode;
    }

    public EntityApproval() {
    }

//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "Id")
//    private Long id;
    @Column(name = "Name")
    private String name;
    @Column(name = "Description")
    private String description;
    @Column(name = "ApprovalFor", nullable = false)
    private String approvalFor;
    @Column(name = "CreatedBy", nullable = false)
    private String createdBy;
    @Column(name = "DateCreated", nullable = false, columnDefinition = "DATETIME")
    // @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dateCreated;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ApprovalCode", nullable = false)
    private String approvalCode;
    @Column(name = "ApprovalStatus")
    private com.gsi.lib.util.Enum.ApprovalStatus approvalStatus = com.gsi.lib.util.Enum.ApprovalStatus.Pending;
    @Column(name = "ApprovedBy", nullable = true)
    private String approvedBy;
    @Column(name = "DateApproved", nullable = true, columnDefinition = "DATETIME")
    //  @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dateApproved;
    @Column(name = "Comment", nullable = true)
    private String comment;
//    @OneToOne(mappedBy = "mandateApproval")
//    private GSIMandate mandate;
    @OneToOne(mappedBy = "transactionApproval")
    private GSITransaction transaction;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getApprovalFor() {
        return approvalFor;
    }

    public void setApprovalFor(String approvalFor) {
        this.approvalFor = approvalFor;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getApprovalCode() {
        return approvalCode;
    }

    public void setApprovalCode(String approvalCode) {
        this.approvalCode = approvalCode;
    }

    public Enum.ApprovalStatus getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(Enum.ApprovalStatus approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public Date getDateApproved() {
        return dateApproved;
    }

    public void setDateApproved(Date dateApproved) {
        this.dateApproved = dateApproved;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

//    public GSIMandate getMandate() {
//        return mandate;
//    }
//
//    public void setMandate(GSIMandate mandate) {
//        this.mandate = mandate;
//    }

    public GSITransaction getTransaction() {
        return transaction;
    }

    public void setTransaction(GSITransaction transaction) {
        this.transaction = transaction;
    }

//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }

}
