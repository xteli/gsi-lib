/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsi.lib.entities;

import com.gsi.lib.util.Enum;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author chineduojiteli
 */
@Entity
@Table(name = "GSITransaction")
public class GSITransaction extends BaseObject implements Serializable {

    @ManyToOne
    @JoinColumn(name = "MandateRef")
//    @Column(name = "MandateRef", nullable = false)
    private GSIMandate mandate;
    @Column(name = "MandateSubjectID", nullable = false)
    private String mandateSubjectID;
    @Column(name = "TransactionRef", nullable = false)
    private String transactionRef;
    @Column(name = "Channel", nullable = false)
    private com.gsi.lib.util.Enum.Channel channel;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "RequestTime", nullable = false)
    private Date requestTime;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ResponseTime", nullable = true)
    private Date responseTime;
    @Column(name = "ResponseCode", nullable = false)
    private String responseCode;
    @Column(name = "ResponseDescription", nullable = false)
    private String responseDescription;
    @Column(name = "RepaymentAccountNumber", nullable = false)
    private String repaymentAccountNumber;
    @Column(name = "RepaymentBankCode", nullable = false)
    private String repaymentBankCode;
    @Column(name = "Amount")
    private BigDecimal amount;
    @Column(name = "CurrencyId", nullable = false)
    private int currencyId;
    @Column(name = "CorporateId", nullable = false)
    private String corporateId;

    //mandate instruction details
    @Column(name = "UploadFileName", nullable = false)
    private String uploadFileName;
    @Column(name = "UploadFileExtension", nullable = false)
    private String uploadFileExtension;
    @Column(name = "UploadFileLocation", nullable = false)
    private String uploadFileLocation;
    @Column(name = "UploadFileBase64", nullable = true, columnDefinition = "LONGTEXT")
    private String uploadFileBase64;
    @Column(name = "IsFileUploaded", nullable = false)
    private boolean isFileUploaded = false;

    //nibss specific details
    @Column(name = "GSITransactonID", nullable = false)
    private String gsiTransactonID;
    @Column(name = "NIPResponseCode", nullable = false)
    private String nipResponseCode;
    //reversal details
    @Column(name = "ReversalRef", nullable = true)
    private String reversalRef;
    @Column(name = "Reversible", nullable = true)
    private boolean isReversible = false;
    @Column(name = "ReversalDateTime", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date reversalDate;
    @Column(name = "ReversalResponseCode", nullable = true)
    private String reversalResponseCode;
    @Column(name = "ReversalResponseDescription", nullable = true)
    private String reversalResponseDescription;
    @Column(name = "ReversalReason", nullable = true)
    private String reversalReason;

    //manage approval flow
    @OneToOne
    @JoinColumn(name = "ApprovalCode")
    private EntityApproval transactionApproval;

    public GSIMandate getMandate() {
        return mandate;
    }

    public void setMandate(GSIMandate mandate) {
        this.mandate = mandate;
    }

    public String getMandateSubjectID() {
        return mandateSubjectID;
    }

    public void setMandateSubjectID(String mandateSubjectID) {
        this.mandateSubjectID = mandateSubjectID;
    }

    public String getTransactionRef() {
        return transactionRef;
    }

    public void setTransactionRef(String transactionRef) {
        this.transactionRef = transactionRef;
    }

    public Enum.Channel getChannel() {
        return channel;
    }

    public void setChannel(Enum.Channel channel) {
        this.channel = channel;
    }

    public Date getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(Date requestTime) {
        this.requestTime = requestTime;
    }

    public Date getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(Date responseTime) {
        this.responseTime = responseTime;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseDescription() {
        return responseDescription;
    }

    public void setResponseDescription(String responseDescription) {
        this.responseDescription = responseDescription;
    }

    public String getRepaymentAccountNumber() {
        return repaymentAccountNumber;
    }

    public void setRepaymentAccountNumber(String repaymentAccountNumber) {
        this.repaymentAccountNumber = repaymentAccountNumber;
    }

    public String getRepaymentBankCode() {
        return repaymentBankCode;
    }

    public void setRepaymentBankCode(String repaymentBankCode) {
        this.repaymentBankCode = repaymentBankCode;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public int getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(int currencyId) {
        this.currencyId = currencyId;
    }

    public String getReversalRef() {
        return reversalRef;
    }

    public void setReversalRef(String reversalRef) {
        this.reversalRef = reversalRef;
    }

    public boolean isIsReversible() {
        return isReversible;
    }

    public void setIsReversible(boolean isReversible) {
        this.isReversible = isReversible;
    }

    public Date getReversalDate() {
        return reversalDate;
    }

    public void setReversalDate(Date reversalDate) {
        this.reversalDate = reversalDate;
    }

    public String getReversalResponseCode() {
        return reversalResponseCode;
    }

    public void setReversalResponseCode(String reversalResponseCode) {
        this.reversalResponseCode = reversalResponseCode;
    }

    public String getGsiTransactonID() {
        return gsiTransactonID;
    }

    public void setGsiTransactonID(String gsiTransactonID) {
        this.gsiTransactonID = gsiTransactonID;
    }

    public String getNipResponseCode() {
        return nipResponseCode;
    }

    public void setNipResponseCode(String nipResponseCode) {
        this.nipResponseCode = nipResponseCode;
    }

    public String getUploadFileName() {
        return uploadFileName;
    }

    public void setUploadFileName(String uploadFileName) {
        this.uploadFileName = uploadFileName;
    }

    public String getUploadFileExtension() {
        return uploadFileExtension;
    }

    public void setUploadFileExtension(String uploadFileExtension) {
        this.uploadFileExtension = uploadFileExtension;
    }

    public String getUploadFileLocation() {
        return uploadFileLocation;
    }

    public void setUploadFileLocation(String uploadFileLocation) {
        this.uploadFileLocation = uploadFileLocation;
    }

    public boolean isIsFileUploaded() {
        return isFileUploaded;
    }

    public void setIsFileUploaded(boolean isFileUploaded) {
        this.isFileUploaded = isFileUploaded;
    }

    public EntityApproval getTransactionApproval() {
        return transactionApproval;
    }

    public void setTransactionApproval(EntityApproval transactionApproval) {
        this.transactionApproval = transactionApproval;
    }

    public String getReversalReason() {
        return reversalReason;
    }

    public void setReversalReason(String reversalReason) {
        this.reversalReason = reversalReason;
    }

    public String getReversalResponseDescription() {
        return reversalResponseDescription;
    }

    public void setReversalResponseDescription(String reversalResponseDescription) {
        this.reversalResponseDescription = reversalResponseDescription;
    }

    public String getCorporateId() {
        return corporateId;
    }

    public void setCorporateId(String corporateId) {
        this.corporateId = corporateId;
    }

    public String getUploadFileBase64() {
        return uploadFileBase64;
    }

    public void setUploadFileBase64(String uploadFileBase64) {
        this.uploadFileBase64 = uploadFileBase64;
    }
    
    

}
