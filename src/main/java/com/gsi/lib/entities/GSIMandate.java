/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsi.lib.entities;

import com.gsi.lib.util.Enum.MandateStatus;
import com.gsi.lib.util.Enum.MandateType;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author chineduojiteli
 */
@Entity
@Table(name = "GSIMandate")
public class GSIMandate extends BaseObject implements Serializable {

    public GSIMandate(String mandateRef) {
        this.mandateRef = mandateRef;
    }

    public GSIMandate() {
    }

    @Column(name = "MandateReference", nullable = false)
    private String mandateRef;
    @Column(name = "CorporateId", nullable = false)
    private String corporateId;
    @Column(name = "MandateType", nullable = false)
    private MandateType mandateType;
    @Column(name = "MandateSubjectID", nullable = false)
    private String mandateSubjectID;
    @Column(name = "CurrencyId", nullable = false)
    private int currencyId;
    @Column(name = "Amount")
    private BigDecimal amount;
    @Column(name = "CRMSReference", nullable = true)
    private String CRMSReference;
    @Column(name = "MandateStatus", nullable = false)
    private MandateStatus mandateStatus = MandateStatus.Inactive;
    @Column(name = "IsFirstTime")
    private boolean isFirstTime = true;

    //repayment account details
    @Column(name = "RepaymentAccountNumber", nullable = true)
    private String repaymentAccountNumber;
    @Column(name = "RepaymentInstitutionCode", nullable = true)
    private String repaymentInstitutionCode;

    //mandate instruction details
    @Column(name = "MandateFileName", nullable = false)
    private String mandateFileName;
    @Column(name = "MandateFileExtension", nullable = false)
    private String mandateFileExtension;
    @Column(name = "MandateFileLocation", nullable = false)
    private String mandateFileLocation;
    @Column(name = "IsMandateFileUploaded", nullable = false)
    private boolean isMandateFileUploaded = false;
    @Column(name = "MandateFileBase64", nullable = true, columnDefinition = "LONGTEXT")
    private String mandateFileBase64;

    //details of closed mandate
    @Column(name = "ReasonForClosure", nullable = true)
    private String reasonForClosure;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DateClosed", nullable = true)
    private Date dateClosed;

    @Column(name = "IsAvailable", nullable = false)
    private boolean isAvailable = false;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DateAvailable", nullable = true)
    private Date dateAvailable;

    //manage approval flow
//    @OneToOne
//    @JoinColumn(name = "ApprovalCode")
//    private EntityApproval mandateApproval;
    //contains all transactions performed on this mandate ref
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "MandateRef")
    private Set<GSITransaction> transactions;

    public String getMandateRef() {
        return mandateRef;
    }

    public void setMandateRef(String mandateRef) {
        this.mandateRef = mandateRef;
    }

    public String getCorporateId() {
        return corporateId;
    }

    public void setCorporateId(String corporateId) {
        this.corporateId = corporateId;
    }

    public MandateType getMandateType() {
        return mandateType;
    }

    public void setMandateType(MandateType mandateType) {
        this.mandateType = mandateType;
    }

    public String getMandateSubjectID() {
        return mandateSubjectID;
    }

    public void setMandateSubjectID(String mandateSubjectID) {
        this.mandateSubjectID = mandateSubjectID;
    }

    public int getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(int currencyId) {
        this.currencyId = currencyId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCRMSReference() {
        return CRMSReference;
    }

    public void setCRMSReference(String CRMSReference) {
        this.CRMSReference = CRMSReference;
    }

    public MandateStatus getMandateStatus() {
        return mandateStatus;
    }

    public void setMandateStatus(MandateStatus mandateStatus) {
        this.mandateStatus = mandateStatus;
    }

    public boolean isIsFirstTime() {
        return isFirstTime;
    }

    public void setIsFirstTime(boolean isFirstTime) {
        this.isFirstTime = isFirstTime;
    }

    public String getRepaymentAccountNumber() {
        return repaymentAccountNumber;
    }

    public void setRepaymentAccountNumber(String repaymentAccountNumber) {
        this.repaymentAccountNumber = repaymentAccountNumber;
    }

    public String getRepaymentInstitutionCode() {
        return repaymentInstitutionCode;
    }

    public void setRepaymentInstitutionCode(String repaymentInstitutionCode) {
        this.repaymentInstitutionCode = repaymentInstitutionCode;
    }

    public String getMandateFileName() {
        return mandateFileName;
    }

    public void setMandateFileName(String mandateFileName) {
        this.mandateFileName = mandateFileName;
    }

    public String getMandateFileExtension() {
        return mandateFileExtension;
    }

    public void setMandateFileExtension(String mandateFileExtension) {
        this.mandateFileExtension = mandateFileExtension;
    }

    public boolean isIsMandateFileUploaded() {
        return isMandateFileUploaded;
    }

    public void setIsMandateFileUploaded(boolean isMandateFileUploaded) {
        this.isMandateFileUploaded = isMandateFileUploaded;
    }

    public String getReasonForClosure() {
        return reasonForClosure;
    }

    public void setReasonForClosure(String reasonForClosure) {
        this.reasonForClosure = reasonForClosure;
    }

    public Date getDateClosed() {
        return dateClosed;
    }

    public void setDateClosed(Date dateClosed) {
        this.dateClosed = dateClosed;
    }

//    public EntityApproval getMandateApproval() {
//        return mandateApproval;
//    }
//
//    public void setMandateApproval(EntityApproval mandateApproval) {
//        this.mandateApproval = mandateApproval;
//    }
    public String getMandateFileLocation() {
        return mandateFileLocation;
    }

    public void setMandateFileLocation(String mandateFileLocation) {
        this.mandateFileLocation = mandateFileLocation;
    }

    public Date getDateAvailable() {
        return dateAvailable;
    }

    public void setDateAvailable(Date dateAvailable) {
        this.dateAvailable = dateAvailable;
    }

    public boolean isIsAvailable() {
        return isAvailable;
    }

    public void setIsAvailable(boolean isAvailable) {
        this.isAvailable = isAvailable;
    }

    public Set<GSITransaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(Set<GSITransaction> transactions) {
        this.transactions = transactions;
    }

    public String getMandateFileBase64() {
        return mandateFileBase64;
    }

    public void setMandateFileBase64(String mandateFileBase64) {
        this.mandateFileBase64 = mandateFileBase64;
    }
    
    

}
