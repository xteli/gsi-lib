/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsi.lib.util;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author xteli
 */
public class Enum {

    public enum OperationName {

        Ping,
        CreateGSIMandate,
        UpdateGSIMandate,
        CloseGSIMandate,
        PendingApproval,
        ApproveMandate,
        DisapproveMandate,
        Transaction,
        TransactionStatus,
        Reversal,
        ReversalStatus,
        ApprovalStatus,
        Report;
    }

    public enum Channel {

        Nil(0),
        BankTeller(1),
        InternetBanking(2),
        Mobile(3),
        POS(4),
        ATM(5),
        MerchantWebPortal(6),
        ThirdPartyPaymentPlatform(7),
        USSD(8),
        Others(9);

        private int enumValue;

        Channel(int enumValue) {
            this.enumValue = enumValue;
        }

        public int getEnumValue() {
            return enumValue;
        }

        private static final Map<Integer, Channel> channelMap = new HashMap<>();

        static {
            for (Channel channel : Channel.values()) {
                channelMap.put(channel.enumValue, channel);
            }
        }

        public static Channel toEnum(int i) {
            Channel pChannel = channelMap.get(i);
            return pChannel;
        }

    }

    public enum ApprovalStatus {

        Pending(0),
        Approved(1),
        Disapproved(2);

        private int enumValue;

        ApprovalStatus(int enumValue) {
            this.enumValue = enumValue;
        }

        public int getEnumValue() {
            return enumValue;
        }

        private static final Map<Integer, ApprovalStatus> approvalMap = new HashMap<>();

        static {
            for (ApprovalStatus approv : ApprovalStatus.values()) {
                approvalMap.put(approv.enumValue, approv);
            }
        }

        public static ApprovalStatus toEnum(int i) {
            ApprovalStatus approv = approvalMap.get(i);
            return approv;
        }

    }

    public enum MandateStatus {

        Inactive(0),
        Active(1),
        Closed(2);

        private int enumValue;

        MandateStatus(int enumValue) {
            this.enumValue = enumValue;
        }

        public int getEnumValue() {
            return enumValue;
        }

        private static final Map<Integer, MandateStatus> mandStatusMap = new HashMap<>();

        static {
            for (MandateStatus mandStat : MandateStatus.values()) {
                mandStatusMap.put(mandStat.enumValue, mandStat);
            }
        }

        public static MandateStatus toEnum(int i) {
            MandateStatus mandStat = mandStatusMap.get(i);
            return mandStat;
        }

    }

    public enum MandateType {

        Nil(0),
        Individual(1),
        Corporate(2);

        private int enumValue;

        MandateType(int enumValue) {
            this.enumValue = enumValue;
        }

        public int getEnumValue() {
            return enumValue;
        }

        private static final Map<Integer, MandateType> mandTypeMap = new HashMap<>();

        static {
            for (MandateType mandType : MandateType.values()) {
                mandTypeMap.put(mandType.enumValue, mandType);
            }
        }

        public static MandateType toEnum(int i) {
            MandateType mandType = mandTypeMap.get(i);
            return mandType;
        }

    }

}
