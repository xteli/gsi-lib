/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsi.lib.dao;

import com.gsi.lib.entities.Corporate;
import com.gsi.lib.entities.Currency;
import com.gsi.lib.entities.EntityApproval;
import com.gsi.lib.entities.GSIMandate;
import com.gsi.lib.entities.GSITransaction;
import com.gsi.lib.entities.Institution;
import com.gsi.lib.entities.Ping;
import com.gsi.lib.util.Enum.ApprovalStatus;
import com.gsi.lib.util.Enum.MandateStatus;
import com.gsi.lib.util.ResponseCode;
import java.text.SimpleDateFormat;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author chineduojiteli
 */
public class GSISystem extends CoreSystem {

    public Ping isServiceAvailable() {
        Ping ping = null;
        Session session = BuildSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            //the below must be wrapped in a transaction
            Criteria criteria = session.createCriteria(Ping.class);
            ping = (Ping) criteria.uniqueResult();
            transaction.commit();
        } catch (HibernateException hex) {
            if (transaction != null) {
                transaction.rollback();
            }
            hex.printStackTrace();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return ping;
    }

    public Institution retrieveInstitution(String bankCode) {
        Institution institution = null;
        Session session = null;
        Transaction transaction = null;
        try {
            session = BuildSession();
            transaction = session.beginTransaction();
            //the below must be wrapped in a transaction
            Criteria criteria = session.createCriteria(Institution.class);
            if (bankCode != null && !"".equals(bankCode)) {
                criteria.add(Restrictions.eq("bankCode", bankCode));
            }

            institution = (Institution) criteria.uniqueResult();
            transaction.commit();
        } catch (HibernateException hex) {
            if (transaction != null) {
                transaction.rollback();
            }
            hex.printStackTrace();

        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return institution;
    }

    public Institution retrieveInstitutionByCode(String instCode) {
        Institution institution = null;
        Session session = null;
        Transaction transaction = null;
        try {
            session = BuildSession();
            transaction = session.beginTransaction();
            //the below must be wrapped in a transaction
            Criteria criteria = session.createCriteria(Institution.class);
            if (instCode != null && !"".equals(instCode)) {
                criteria.add(Restrictions.eq("institutionCode", instCode));
            }

            institution = (Institution) criteria.uniqueResult();
            transaction.commit();
        } catch (HibernateException hex) {
            if (transaction != null) {
                transaction.rollback();
            }
            hex.printStackTrace();

        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return institution;
    }

    public Corporate retrieveCorporate(String corporateId) {
        Corporate corporate = null;
        Session session = null;
        Transaction transaction = null;
        try {
            session = BuildSession();
            transaction = session.beginTransaction();
            //the below must be wrapped in a transaction
            Criteria criteria = session.createCriteria(Corporate.class);
            if (corporateId != null && !"".equals(corporateId)) {
                criteria.add(Restrictions.eq("corporateId", corporateId));
            }

            corporate = (Corporate) criteria.uniqueResult();
            transaction.commit();
        } catch (HibernateException hex) {
            if (transaction != null) {
                transaction.rollback();
            }
            hex.printStackTrace();

        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return corporate;
    }

    public Currency retrieveCurrency(long currencyId) {
        Currency currency = null;
        Session session = null;
        Transaction transaction = null;
        try {
            session = BuildSession();
            transaction = session.beginTransaction();
            //the below must be wrapped in a transaction
            Criteria criteria = session.createCriteria(Currency.class);
            if (currencyId > 0) {
                criteria.add(Restrictions.eq("id", currencyId));
            }

            currency = (Currency) criteria.uniqueResult();
            transaction.commit();
        } catch (HibernateException hex) {
            if (transaction != null) {
                transaction.rollback();
            }
            hex.printStackTrace();

        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return currency;
    }

    public GSIMandate retrieveGSIMandate(String mandateOwner, String mandateRef, String subjectId) {
        GSIMandate mandate = null;
        Session session = BuildSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            //the below must be wrapped in a transaction
            Criteria criteria = session.createCriteria(GSIMandate.class).addOrder(Order.desc("dateCreated"));

            if (mandateOwner != null && !"".equals(mandateOwner)) {
                criteria.add(Restrictions.eq("mandateOwner", mandateOwner));
            }

            if (mandateRef != null && !"".equals(mandateRef)) {
                criteria.add(Restrictions.eq("mandateRef", mandateRef));
            }

            if (subjectId != null && !"".equals(subjectId)) {
                criteria.add(Restrictions.eq("mandateSubjectID", subjectId));
            }

            mandate = (GSIMandate) criteria.uniqueResult();
            transaction.commit();
        } catch (Exception hex) {
            if (transaction != null) {
                transaction.rollback();
            }
            hex.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return mandate;
    }

    public GSITransaction retrieveGSITransaction(String mandateOwner, String transRef) {
        GSITransaction gsiTransaction = null;
        Session session = BuildSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            //the below must be wrapped in a transaction
            Criteria criteria = session.createCriteria(GSITransaction.class).addOrder(Order.desc("dateCreated"));

            if (mandateOwner != null && !"".equals(mandateOwner)) {
                criteria.add(Restrictions.eq("mandateOwner", mandateOwner));
            }

            if (transRef != null && !"".equals(transRef)) {
                criteria.add(Restrictions.eq("transactionRef", transRef));
            }

            gsiTransaction = (GSITransaction) criteria.uniqueResult();
            transaction.commit();
        } catch (Exception hex) {
            if (transaction != null) {
                transaction.rollback();
            }
            hex.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return gsiTransaction;
    }

    public GSITransaction retrieveGSIReversalTransaction(String mandateOwner, String transRef) {
        GSITransaction gsiTransaction = null;
        Session session = BuildSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            //the below must be wrapped in a transaction
            Criteria criteria = session.createCriteria(GSITransaction.class)
                    .add(Restrictions.eq("responseCode", ResponseCode.SUCCESSFUL))
                    .add(Restrictions.eq("nipResponseCode", ResponseCode.SUCCESSFUL))
                    .add(Restrictions.eq("isReversible", true));

            if (mandateOwner != null && !"".equals(mandateOwner)) {
                criteria.add(Restrictions.eq("mandateOwner", mandateOwner));
            }

            if (transRef != null && !"".equals(transRef)) {
                criteria.add(Restrictions.eq("transactionRef", transRef));
            }

            gsiTransaction = (GSITransaction) criteria.uniqueResult();
            transaction.commit();
        } catch (Exception hex) {
            if (transaction != null) {
                transaction.rollback();
            }
            hex.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return gsiTransaction;
    }

    public List<GSIMandate> retrieveUndueGSIMandate() {
        List<GSIMandate> mandateList = null;
        Session session = BuildSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            //the below must be wrapped in a transaction
            Criteria criteria = session.createCriteria(GSIMandate.class).addOrder(Order.desc("dateCreated"))
                    .createAlias("approval", "approval")
                    .add(Restrictions.eq("approval.approvalStatus", ApprovalStatus.Approved))
                    .add(Restrictions.ne("mandateStatus", MandateStatus.Closed))
                    .add(Restrictions.eq("isAvailable", false));

            mandateList = criteria.list();
            transaction.commit();
        } catch (Exception hex) {
            if (transaction != null) {
                transaction.rollback();
            }
            hex.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return mandateList;
    }

    public List<EntityApproval> retrievePendingApprovals(String startDate, String endDate, String instCode) {
        List<EntityApproval> tranList = null;
        Session session = BuildSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            //the below must be wrapped in a transaction
            Criteria criteria = session.createCriteria(EntityApproval.class).addOrder(Order.desc("dateCreated"));

            if (startDate != null && endDate != null) {
                criteria.add(Restrictions.between("dateCreated", new SimpleDateFormat("yyyy-MM-dd").parse(startDate), new SimpleDateFormat("yyyy-MM-dd").parse(endDate)));
            }

            if (instCode != null && !"".equals(instCode)) {
                criteria.add(Restrictions.eq("approvalFor", instCode));
            }

            tranList = criteria.list();
            transaction.commit();
        } catch (Exception hex) {
            hex.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return tranList;
    }

    public EntityApproval retrieveGSIMandateForApproval(String ref, String mandateOwner) {
        EntityApproval approval = null;
        Session session = BuildSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            //the below must be wrapped in a transaction
            Criteria criteria = session.createCriteria(EntityApproval.class)
                    .add(Restrictions.eq("approvalStatus", ApprovalStatus.Pending))
                    .add(Restrictions.isNotNull("approvalCode"))
                    .createAlias("mandate", "mandate");
            if (ref != null && !"".equals(ref)) {
                criteria.add(Restrictions.eq("mandate.mandateRef", ref));
            }

            if (mandateOwner != null && !"".equals(mandateOwner)) {
                criteria.add(Restrictions.eq("approvalFor", mandateOwner));
            }

            approval = (EntityApproval) criteria.uniqueResult();
            transaction.commit();
        } catch (Exception hex) {
            if (transaction != null) {
                transaction.rollback();
            }
            hex.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return approval;
    }

    public EntityApproval retrieveGSITransactionForApproval(String ref, String mandateOwner) {
        EntityApproval approval = null;
        Session session = BuildSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            //the below must be wrapped in a transaction
            Criteria criteria = session.createCriteria(EntityApproval.class)
                    .add(Restrictions.eq("approvalStatus", ApprovalStatus.Pending))
                    .add(Restrictions.isNotNull("approvalCode"))
                    .createAlias("transaction", "transaction");
            if (ref != null && !"".equals(ref)) {
                criteria.add(Restrictions.eq("transaction.transactionRef", ref));
            }

            if (mandateOwner != null && !"".equals(mandateOwner)) {
                criteria.add(Restrictions.eq("approvalFor", mandateOwner));
            }

            approval = (EntityApproval) criteria.uniqueResult();
            transaction.commit();
        } catch (Exception hex) {
            if (transaction != null) {
                transaction.rollback();
            }
            hex.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return approval;
    }

    public EntityApproval retrieveGSIReversalForApproval(String ref, String mandateOwner) {
        EntityApproval approval = null;
        Session session = BuildSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            //the below must be wrapped in a transaction
            Criteria criteria = session.createCriteria(EntityApproval.class)
                    .add(Restrictions.eq("approvalStatus", ApprovalStatus.Pending))
                    .add(Restrictions.isNotNull("approvalCode"))
                    .createAlias("transaction", "transaction")
                    .add(Restrictions.eq("transaction.isReversible", true))
                    .add(Restrictions.eq("transaction.reversalResponseCode", ResponseCode.REQUEST_IN_PROGRESS));
            if (ref != null && !"".equals(ref)) {
                criteria.add(Restrictions.eq("transaction.transactionRef", ref));
            }

            if (mandateOwner != null && !"".equals(mandateOwner)) {
                criteria.add(Restrictions.eq("approvalFor", mandateOwner));
            }

            approval = (EntityApproval) criteria.uniqueResult();
            transaction.commit();
        } catch (Exception hex) {
            if (transaction != null) {
                transaction.rollback();
            }
            hex.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return approval;
    }

    public List<GSITransaction> retrieveApprovedGSITransactions() {
        List<GSITransaction> tranList = null;
        Session session = BuildSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            //the below must be wrapped in a transaction
            Criteria criteria = session.createCriteria(GSITransaction.class).addOrder(Order.asc("dateCreated"));
            criteria.createAlias("approval", "approval");
            criteria.add(Restrictions.eq("approval.approvalStatus", ApprovalStatus.Approved))
                    .add(Restrictions.eq("approval.name", "GSI TRANSACTION INITIATION"))
                    .add(Restrictions.isNotNull("approval.approvalCode"))
                    .add(Restrictions.isNotNull("approval.approvedBy"))
                    .add(Restrictions.isNotNull("approval.dateApproved"));

            tranList = criteria.list();
            transaction.commit();
        } catch (Exception hex) {
            if (transaction != null) {
                transaction.rollback();
            }
            hex.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return tranList;
    }

    public List<GSITransaction> retrieveGSITransactionsForReversal() {
        List<GSITransaction> tranList = null;
        Session session = BuildSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            //the below must be wrapped in a transaction
            Criteria criteria = session.createCriteria(GSITransaction.class)
                    .addOrder(Order.asc("dateCreated"))
                    .add(Restrictions.eq("responseCode", ResponseCode.SUCCESSFUL))
                    .add(Restrictions.eq("nipResponseCode", ResponseCode.SUCCESSFUL))
                    .add(Restrictions.isNotNull("gsiTransactonID"))
                    .add(Restrictions.eq("isReversible", true));
            criteria.createAlias("approval", "approval");
            criteria.add(Restrictions.eq("approval.approvalStatus", ApprovalStatus.Approved))
                    .add(Restrictions.eq("approval.name", "GSI REVERSAL INITIATION"))
                    .add(Restrictions.isNotNull("approval.approvalCode"))
                    .add(Restrictions.isNotNull("approval.approvedBy"))
                    .add(Restrictions.isNotNull("approval.dateApproved"));

            tranList = criteria.list();
            transaction.commit();
        } catch (Exception hex) {
            if (transaction != null) {
                transaction.rollback();
            }
            hex.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return tranList;
    }

//    public GSIMandate retrieveGSIMandateForApproval_OLD(String ref, String mandateOwner) {
//        GSIMandate mandate = null;
//        Session session = BuildSession();
//        Transaction transaction = null;
//        try {
//            transaction = session.beginTransaction();
//            //the below must be wrapped in a transaction
//            Criteria criteria = session.createCriteria(GSIMandate.class).createAlias("approval", "approval")
//                    .add(Restrictions.eq("approval.approvalStatus", ApprovalStatus.Pending))
//                    .add(Restrictions.isNotNull("approvalCode"));
//            if (ref != null && !"".equals(ref)) {
//                criteria.add(Restrictions.eq("mandateRef", ref));
//            }
//
//            if (mandateOwner != null && !"".equals(mandateOwner)) {
//                criteria.add(Restrictions.eq("mandateOwner", mandateOwner));
//            }
//
//            mandate = (GSIMandate) criteria.uniqueResult();
//            transaction.commit();
//        } catch (Exception hex) {
//            if (transaction != null) {
//                transaction.rollback();
//            }
//            hex.printStackTrace();
//        } finally {
//            if (session != null && session.isOpen()) {
//                session.close();
//            }
//        }
//        return mandate;
//    }
}
