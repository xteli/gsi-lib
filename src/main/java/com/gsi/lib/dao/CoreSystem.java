/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsi.lib.dao;

import com.gsi.lib.dbutil.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author chineduojiteli
 */
public class CoreSystem {

    public static SessionFactory getSessionFactory() {
        return HibernateUtil.getSessionFactory();
    }

    public Session BuildSession() {
        SessionFactory sFactory = getSessionFactory();
        Session ses = sFactory.getCurrentSession();
        return ses;
    }

    public static void shutdown() {
        getSessionFactory().close();
    }

    public <T> boolean saveEntity(T entity) {
        boolean saved = false;
        Session builtSession = null;
        Transaction transaction = null;

        try {
            builtSession = BuildSession();
            transaction = builtSession.beginTransaction();
            //the below must be wrapped in a transaction
            builtSession.save(entity);
            transaction.commit();
            saved = true;
        } catch (Exception hex) {
            if (transaction != null) {
                transaction.rollback();
            }
            hex.printStackTrace();

        } finally {
            if (builtSession != null && builtSession.isOpen()) {
                builtSession.close();
            }
        }
        return saved;
    }

    public <T> boolean updateEntity(T entity) {
        boolean updated = false;
        Session builtSession = null;
        Transaction transaction = null;
        try {
            builtSession = BuildSession();
            transaction = builtSession.beginTransaction();
            //the below must be wrapped in a transaction
            builtSession.update(entity);
            transaction.commit();
            updated = true;
        } catch (Exception hex) {
            if (transaction != null) {
                transaction.rollback();
            }
            hex.printStackTrace();

        } finally {
            if (builtSession != null && builtSession.isOpen()) {
                builtSession.close();
            }
        }
        return updated;
    }

    public <T> void deleteEntity(T entity) {
        Session builtSession = null;
        Transaction transaction = null;
        try {
            builtSession = BuildSession();
            transaction = builtSession.beginTransaction();
            //the below must be wrapped in a transaction
            builtSession.delete(entity);
            transaction.commit();
        } catch (Exception hex) {
            if (transaction != null) {
                transaction.rollback();
            }
            hex.printStackTrace();

        } finally {
            if (builtSession != null && builtSession.isOpen()) {
                builtSession.close();
            }
        }
    }

}
